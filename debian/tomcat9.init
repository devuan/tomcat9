#!/bin/sh
### BEGIN INIT INFO
# Provides:          tomcat9
# Required-Start:    $local_fs $remote_fs $network
# Required-Stop:     $local_fs $remote_fs $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Tomcat 9
# Description:       The Tomcat 9 servlet engine runs Java Web Archives.
### END INIT INFO

# stuff away, used later
saved_LC_ALL=${LC_ALL+x$LC_ALL}
export saved_LC_ALL

# absolute basics
LC_ALL=C PATH=/sbin:/usr/sbin:/bin:/usr/bin
export LC_ALL PATH
unset LANGUAGE

# exit cleanly if disabled or not installed
test -x /usr/libexec/tomcat9/sysv-start.sh || exit 0
test -x /usr/libexec/tomcat9/sysv-getjre.sh || exit 0
test -x /usr/libexec/tomcat9/tomcat-update-policy.sh || exit 0
test -x /usr/libexec/tomcat9/tomcat-start.sh || exit 0

# Debian/LSB init script foobar
DESC='Tomcat 9 servlet engine'
NAME=tomcat9
readonly DESC NAME
. /lib/init/vars.sh
test -t 0 && VERBOSE=yes
. /lib/lsb/init-functions

# somewhat LSB-compliant exit with failure
if test x"$1" = x"status"; then
	exit_failure_msg() {
		log_failure_msg "$@"
		exit 4
	}
else
	exit_failure_msg() {
		log_failure_msg "$@"
		exit 1
	}
fi

# set defaults for options
CATALINA_HOME=/usr/share/tomcat9
CATALINA_BASE=/var/lib/tomcat9
CATALINA_TMPDIR=/tmp/tomcat9-tmp
export CATALINA_HOME CATALINA_BASE CATALINA_TMPDIR
JAVA_HOME= # determined later if empty
JAVA_OPTS=-Djava.awt.headless=true
JSP_COMPILER= # only used if nonempty
SECURITY_MANAGER=false
export JAVA_HOME JAVA_OPTS JSP_COMPILER SECURITY_MANAGER
UMASK=022
export UMASK
# read options
test -r /etc/default/tomcat9 && . /etc/default/tomcat9
TOMCAT9_DEFAULTS_FILE_READ=1; export TOMCAT9_DEFAULTS_FILE_READ

# ensure the temporary directory exist and change to it
rm -rf "$CATALINA_TMPDIR"
mkdir "$CATALINA_TMPDIR" || \
    exit_failure_msg 'could not create JVM temporary directory'
chown -h tomcat "$CATALINA_TMPDIR"
cd "$CATALINA_TMPDIR"

# figure out the JRE executable catalina.sh will use
# (we need it for start-stop-daemon --exec for reliability)
_RUNJAVA=$(su tomcat -s /bin/sh -c /usr/libexec/tomcat9/sysv-getjre.sh) || \
    _RUNJAVA="FAIL:$?"
case $_RUNJAVA in
('OK<'*'>')
	_RUNJAVA=${_RUNJAVA#'OK<'}
	_RUNJAVA=${_RUNJAVA%'>'}
	;;
(*)
	exit_failure_msg "could not determine JRE: $_RUNJAVA"
	;;
esac

# prepare for actions
case $1 in
(start|stop|restart|force-reload)
	# handled below
	;;
(try-restart|status)
	start-stop-daemon --status --quiet \
	    --pidfile /var/run/tomcat9.pid \
	    --exec "$_RUNJAVA" --user tomcat
	rv=$?
	# clean up stale pidfile if necessary
	(test x"$rv" = x"1" && rm -f /var/run/tomcat9.pid || :)
	# process status result
	case $1 in
	(try-restart)
		test x"$rv" = x"0" || {
			# service is not running, or status is unknown
			log_success_msg "$NAME is not running"
			exit 0
		}
		# service running, restart it
		;;
	(status)
		case $rv in
		(0)
			log_success_msg "$NAME is running"
			;;
		(4)
			log_failure_msg "could not access PID file for $NAME"
			;;
		(*)
			log_failure_msg "$NAME is not running"
			;;
		esac
		exit $rv
		;;
	esac
	;;
(reload|*)
	# not supported
	echo >&2 "Usage: $0 {start|stop|restart|try-restart|force-reload|status}"
	exit 3
	;;
esac

# handle stopping/starting
rv=0

case $1 in
(stop|restart|try-restart|force-reload)
	test x"$VERBOSE" = x"no" || log_daemon_msg "Stopping $DESC"
	start-stop-daemon --stop --quiet \
	    --retry=10 --oknodo --remove-pidfile \
	    --pidfile /var/run/tomcat9.pid \
	    --exec "$_RUNJAVA" --user tomcat
	rv=$?
	test x"$VERBOSE" = x"no" || log_end_msg $rv
	;;
esac

test x"$rv" = x"0" || exit $rv

case $1 in
(start|restart|try-restart|force-reload)
	/usr/libexec/tomcat9/tomcat-update-policy.sh || \
	    exit_failure_msg 'could not regenerating catalina.policy file'
	rm -f /var/run/tomcat9.pid
	test x"$VERBOSE" = x"no" || log_daemon_msg "Starting $DESC"
	start-stop-daemon --start --quiet \
	    --chuid tomcat --umask "$UMASK" \
	    --startas /usr/libexec/tomcat9/sysv-start.sh \
	    --background --make-pidfile \
	    --pidfile /var/run/tomcat9.pid \
	    --exec "$_RUNJAVA" --user tomcat
	rv=$?
	test x"$VERBOSE" = x"no" || log_end_msg $rv
	;;
esac

exit $rv

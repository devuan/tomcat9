#!/bin/sh
#
# SYSVinit script helper to determine the JRE (for start-stop-daemon)
#

. /usr/libexec/tomcat9/tomcat-locate-java.sh
set +e

. /usr/share/tomcat9/bin/setclasspath.sh

if test -n "$_RUNJAVA"; then
	printf "OK<%s>" "$_RUNJAVA"
else
	echo UNSET
fi
